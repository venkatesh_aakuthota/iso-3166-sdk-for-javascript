import DiContainer from './diContainer';
import ListCountriesUseCase from './listCountriesUseCase';
import ListSubdivisionsWithAlpha2CountryCodeUseCase from './listSubdivisionsWithAlpha2CountryCodeUseCase';

/**
 * @class {Iso3166Sdk}
 */
export default class Iso3166Sdk {

    _diContainer:DiContainer;

    constructor() {

        this._diContainer = new DiContainer();

    }

    listCountries():Promise<Array> {

        return this
            ._diContainer
            .get(ListCountriesUseCase)
            .execute();

    }

    listSubdivisionsWithAlpha2CountryCode(alpha2CountryCode):Promise<Array> {

        return this
            ._diContainer
            .get(ListSubdivisionsWithAlpha2CountryCodeUseCase)
            .execute(alpha2CountryCode);

    }

}