/**
 * @class {SubdivisionSynopsisView}
 */
export default class SubdivisionSynopsisView {

    _name:string;

    _code:string;

    /**
     *
     * @param {string} name
     * @param {string} code
     */
    constructor(name:string,
                code:string) {

        if (!name) {
            throw new TypeError('name required');
        }
        this._name = name;

        if (!code) {
            throw new TypeError('code required')
        }
        this._code = code;

    }

    /**
     * @returns {string}
     */
    get name() {
        return this._name;
    }

    /**
     * @returns {string}
     */
    get code() {
        return this._code;
    }

}
